package main

import (
	"fmt"
	"os"
	"os/signal"
	"syscall"
	"time"
)

func main() {
	fmt.Printf("PID= %d\n", os.Getpid())

	signalCh := make(chan os.Signal)
	signal.Notify(signalCh, syscall.SIGQUIT, syscall.SIGTERM, syscall.SIGUSR1, os.Interrupt)

	s := <-signalCh
	fmt.Printf("Received: %v\n", s)

	for i := 0; i < 10; i++ {
		fmt.Print(".")
		time.Sleep(1 * time.Second)
	}

	fmt.Println("\nDONE!")
}
