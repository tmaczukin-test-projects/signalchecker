FROM golang:1.12-alpine AS builder
ENV CGO_ENABLED=0
COPY main.go ./main.go
RUN go build -o /signalchecker main.go

FROM alpine:3.10
ARG STOP_SIGNAL
COPY --from=builder /signalchecker /signalchecker
STOPSIGNAL $STOP_SIGNAL
CMD ["/signalchecker"]
